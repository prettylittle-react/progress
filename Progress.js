// https://codepen.io/melnik909/pen/daePey

import React from 'react';
import PropTypes from 'prop-types';

import './Progress.scss';

/**
 * Progress
 * @description [Description]
 * @example
  <div id="Progress"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Progress, {
        title : 'Example Progress'
    }), document.getElementById("Progress"));
  </script>
 */
class Progress extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'progress';

		this.state = {
			percentage: props.initialAnimation ? 0 : props.percentage
		};
	}

	componentDidMount() {
		if (this.props.initialAnimation) {
			this.initialTimeout = setTimeout(() => {
				this.requestAnimationFrame = window.requestAnimationFrame(() => {
					this.setState({
						percentage: this.props.percentage
					});
				});
			}, 0);
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			percentage: nextProps.percentage
		});
	}

	componentWillUnmount() {
		clearTimeout(this.initialTimeout);
		window.cancelAnimationFrame(this.requestAnimationFrame);
	}

	getBackgroundPadding() {
		if (this.props.background) {
			// default padding to be the same as strokeWidth
			// compare to null because 0 is falsy
			if (this.props.backgroundPadding == null) {
				return this.props.strokeWidth;
			}
			return this.props.backgroundPadding;
		}
		// don't add padding if not displaying background
		return 0;
	}

	getPathDescription() {
		const {size} = this.props;
		const radius = this.getPathRadius();
		const rotation = this.props.counterClockwise ? 1 : 0;

		// Move to center of canvas
		// Relative move to top canvas
		// Relative arc to bottom of canvas
		// Relative arc to top of canvas
		return `
          M ${size / 2},${size / 2}
          m 0,-${radius}
          a ${radius},${radius} ${rotation} 1 1 0,${2 * radius}
          a ${radius},${radius} ${rotation} 1 1 0,-${2 * radius}
        `;
	}

	getPathStyles() {
		const diameter = Math.PI * 2 * this.getPathRadius();
		const truncatedPercentage = Math.min(Math.max(this.state.percentage, 0), 100);
		const dashoffset = ((100 - truncatedPercentage) / 100) * diameter;

		return {
			strokeDasharray: `${diameter}px ${diameter}px`,
			strokeDashoffset: `${this.props.counterClockwise ? -dashoffset : dashoffset}px`
		};
	}

	getPathRadius() {
		const {size} = this.props;
		// the radius of the path is defined to be in the middle, so in order for the path to
		// fit perfectly inside the 100x100 viewBox, need to subtract half the strokeWidth
		return size / 2 - this.props.strokeWidth / 2 - this.getBackgroundPadding();
	}

	render() {
		const {percentage, className, classes, styles, strokeWidth, text, size} = this.props;
		const pathDescription = this.getPathDescription();

		return (
			<svg className={`${classes.root} ${className}`} style={styles.root} viewBox={`0 0 ${size} ${size}`}>
				{this.props.background ? (
					<circle
						className={classes.background}
						style={styles.background}
						cx={size / 2}
						cy={size / 2}
						r={size / 2}
					/>
				) : null}

				<path
					className={classes.trail}
					style={styles.trail}
					d={pathDescription}
					strokeWidth={strokeWidth}
					fillOpacity={0}
				/>

				<path
					className={classes.path}
					d={pathDescription}
					strokeWidth={strokeWidth}
					fillOpacity={0}
					style={{...styles.path, ...this.getPathStyles()}}
				/>

				{text ? (
					<text className={classes.text} style={styles.text} x={size / 2} y={size / 2}>
						{text}
					</text>
				) : null}
			</svg>
		);
	}
}

Progress.defaultProps = {
	size: 50,
	strokeWidth: 3,
	className: '',
	text: null,
	classes: {
		root: 'progress',
		trail: 'progress__trail',
		path: 'progress__path',
		text: 'progress__text',
		background: 'progress__background'
	},
	styles: {
		root: {},
		trail: {},
		path: {},
		text: {},
		background: {}
	},
	background: false,
	backgroundPadding: null,
	initialAnimation: false,
	counterClockwise: false
};

Progress.propTypes = {
	size: PropTypes.number.isRequired,
	percentage: PropTypes.number.isRequired,
	className: PropTypes.string,
	text: PropTypes.string,
	classes: PropTypes.objectOf(PropTypes.string),
	styles: PropTypes.objectOf(PropTypes.object),
	strokeWidth: PropTypes.number,
	background: PropTypes.bool,
	backgroundPadding: PropTypes.number,
	initialAnimation: PropTypes.bool,
	counterClockwise: PropTypes.bool
};

export default Progress;
